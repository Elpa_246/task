package com.example.task.service.impl;

import com.example.task.config.AppConfiguration;
import com.example.task.dto.LoginUserRequestDto;
import com.example.task.dto.UserRequestDto;
import com.example.task.model.User;
import com.example.task.repo.UserRepository;
import com.example.task.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final AppConfiguration appConfiguration;

    @Override
    public Long saveUser(UserRequestDto userRequestDto) {
        User user = appConfiguration.getMapper().map(userRequestDto, User.class);
        return userRepository.save(user).getId();
    }

    @Override
    public String loginUser(LoginUserRequestDto loginUserRequestDto) {
        Optional<User> optionalUser = userRepository.findByEmail(loginUserRequestDto.getEmail());
        optionalUser.ifPresent(user -> {
                    if (!user.getPassword().equals(loginUserRequestDto.getPassword())) {
                        throw new RuntimeException("Password is incorrect");
                    }
                }
        );
        return "Successfully login";
    }

    @Override
    public String loginUser2(LoginUserRequestDto loginUserRequestDto) {
        Optional<List<User>> optionalUser = userRepository.findByEmails(loginUserRequestDto.getEmail());
        log.info("Get users: {}", optionalUser.get());
        optionalUser.ifPresent(users -> {
                    for (User user : users) {
                        if (!user.getPassword().equals(loginUserRequestDto.getPassword())) {
                            throw new RuntimeException("Password is incorrect");
                        }
                    }
                }
        );
        return null;
    }
}
