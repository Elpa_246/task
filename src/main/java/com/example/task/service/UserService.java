package com.example.task.service;

import com.example.task.dto.LoginUserRequestDto;
import com.example.task.dto.UserRequestDto;

public interface UserService {
    Long saveUser(UserRequestDto userRequestDto);

    String loginUser(LoginUserRequestDto loginUserRequestDto);

    String loginUser2(LoginUserRequestDto loginUserRequestDto);
}

