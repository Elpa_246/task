package com.example.task;

import com.example.task.model.User;
import com.example.task.repo.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class TaskApplication implements CommandLineRunner {

	private final UserRepository userRepository;

	public static void main(String[] args) {
		SpringApplication.run(TaskApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		userRepository.save(User.builder()
				.id(1L)
				.name("pael")
				.email("pa@l.com")
				.password("123")
				.build());

	}
}

